package com.BookDetails;

public class Book {
    private int id;
    private String author;
    private String bookName;
    private int ISBN;
    private String publishedDate;


    public Book(int bookId, String bookAuthor, String bookName, int ISBN, String publishedDate){
        this.id = bookId;
        this.author = bookAuthor;
        this.bookName = bookName;
        this.ISBN = ISBN;
        this.publishedDate = publishedDate;
    }
    public int getBookId(){
        return this.id;
    }

    public String getBookAuthor(){
        return this.author;
    }

    public String getBookName(){
        return this.bookName;
    }

    public int getISBN(){
        return this.ISBN;
    }

    public String getPublishDate(){
        return this.publishedDate;
    }
}