package com.BookDetails;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Book> books = new ArrayList<>();
        Book book1 = new Book(001, "Sagor", "Java Learning", 101, "10-05-21");
        Book book2 = new Book(002, "Rahman", "HTML Learning", 102, "20-03-21");
        Book book3 = new Book(003, "Lutfor", "Python Learning", 103, "10-12-11");
        Book book4 = new Book(004, "Naim", "Dart Learning", 104, "13-12-11");
        Book book5 = new Book(005, "Altaf", "CSS Learning", 105, "30-12-15");

        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);
        books.add(book5);

        for (Book book : books){
        System.out.println(book.getBookId());
        System.out.println(book.getBookAuthor());
        System.out.println(book.getBookName());
        System.out.println(book.getISBN());
        System.out.println(book.getPublishDate());
        System.out.println();

        }






    }

}
